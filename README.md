# iptv_spain
Repositorio para crear un listado IPTV de las televisiones de ámbito nacional, autonómico y local del estado español

Para configurar en el addons Simple IPTV del multimedia KODI.

Introduce en el apartado de m3u esta dirección:

https://gitlab.com/susobaco/iptv_spain/raw/master/iptv_spain.m3u

desactivando el cacheo local.

Para los iconos, puedes descargarlos y configurar la ruta local.
